package by.dev.madhead.kotlin_native_lambda.aws

import kotlinx.serialization.Serializable

@Serializable
data class ProxyIntegrationResponse(
        val isBase64Encoded: Boolean = false,
        val statusCode: Int,
        val headers: Map<String, String>? = null,
        val multiValueHeaders: Map<String, List<String>>? = null,
        val body: String? = null
)
