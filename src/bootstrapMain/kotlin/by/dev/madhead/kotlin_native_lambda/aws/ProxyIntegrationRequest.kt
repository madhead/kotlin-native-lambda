package by.dev.madhead.kotlin_native_lambda.aws

import kotlinx.serialization.Serializable

@Serializable
data class ProxyIntegrationRequest(
        val resource: String,
        val path: String,
        val httpMethod: String,
        val headers: Map<String, String>?,
        val multiValueHeaders: Map<String, List<String>>?,
        val queryStringParameters: Map<String, String>?,
        val multiValueQueryStringParameters: Map<String, List<String>>?,
        val pathParameters: Map<String, String>?,
        val stageVariables: Map<String, String>?,
        val body: String?,
        val isBase64Encoded: Boolean?
)
