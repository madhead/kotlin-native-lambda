package by.dev.madhead.kotlin_native_lambda

import by.dev.madhead.kotlin_native_lambda.aws.ProxyIntegrationRequest
import by.dev.madhead.kotlin_native_lambda.aws.ProxyIntegrationResponse
import io.ktor.client.HttpClient
import io.ktor.client.call.call
import io.ktor.client.engine.curl.Curl
import io.ktor.http.ContentType
import io.ktor.http.HttpMethod
import io.ktor.http.content.TextContent
import kotlinx.cinterop.toKString
import kotlinx.coroutines.io.readRemaining
import kotlinx.coroutines.runBlocking
import kotlinx.io.charsets.Charsets
import kotlinx.io.core.readText
import kotlinx.serialization.json.Json
import platform.posix.getenv

fun main() = runBlocking {
    val client = HttpClient(Curl)

    while (true) {
        // Get next request to process in a long-polling style
        val invocation = client.call("http://${getenv("AWS_LAMBDA_RUNTIME_API")!!.toKString()}/2018-06-01/runtime/invocation/next") {
            method = HttpMethod.Get
        }

        // Parse request
        val invocationId = invocation.response.headers["Lambda-Runtime-Aws-Request-Id"]
        val payload = invocation.response.content.readRemaining().readText(Charsets.UTF_8)
        val proxyIntegrationRequest =
                try {
                    Json.nonstrict.parse(ProxyIntegrationRequest.serializer(), payload)
                } catch (e: Exception) {
                    client.call("http://${getenv("AWS_LAMBDA_RUNTIME_API")!!.toKString()}/2018-06-01/runtime/invocation/$invocationId/response") {
                        method = HttpMethod.Post
                        body = TextContent(
                                Json.nonstrict.stringify(
                                        ProxyIntegrationResponse.serializer(),
                                        ProxyIntegrationResponse(statusCode = 400)
                                ),
                                ContentType.Application.Json
                        )
                    }
                }

        // Log the request
        println(proxyIntegrationRequest)

        // Echo the input
        client.call("http://${getenv("AWS_LAMBDA_RUNTIME_API")!!.toKString()}/2018-06-01/runtime/invocation/$invocationId/response") {
            method = HttpMethod.Post
            body = TextContent(
                    Json.nonstrict.stringify(
                            ProxyIntegrationResponse.serializer(),
                            ProxyIntegrationResponse(
                                    statusCode = 200,
                                    headers = mapOf(
                                            "Content-Type" to "text/plain"
                                    ),
                                    body = proxyIntegrationRequest.toString()
                            )
                    ),
                    ContentType.Application.Json
            )
        }
    }
}
