plugins {
    kotlin("multiplatform").version("1.3.21")
    id("kotlinx-serialization").version("1.3.21")
}

repositories {
    jcenter()
    maven("https://kotlin.bintray.com/kotlinx")
}

kotlin {
    linuxX64("bootstrap") {
        binaries {
            executable("bootstrap") {
                entryPoint = "by.dev.madhead.kotlin_native_lambda.main"
            }
        }
    }
}

dependencies {
    val bootstrapMainImplementation by configurations

    bootstrapMainImplementation("io.ktor:ktor-client-curl-linuxx64:1.1.3")
    bootstrapMainImplementation("io.ktor:ktor-client-json-linuxx64:1.1.3")
}

tasks {
    wrapper {
        gradleVersion = "5.3"
        distributionType = Wrapper.DistributionType.ALL
    }
}
