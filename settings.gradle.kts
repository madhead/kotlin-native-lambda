import org.gradle.api.internal.FeaturePreviews

rootProject.name = "kotlin-native-lambda"

enableFeaturePreview(FeaturePreviews.Feature.GRADLE_METADATA.name)

pluginManagement {
    repositories {
        gradlePluginPortal()
        add(jcenter())
    }

    resolutionStrategy {
        eachPlugin {
            if (requested.id.id == "kotlinx-serialization") {
                useModule("org.jetbrains.kotlin:kotlin-serialization:${requested.version}")
            }
        }
    }
}
